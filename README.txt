CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Use this module along with robots.txt module. This module allows you check the specific user who has modified Robots.txt file. The Module allows you to download the robots.txt file from a list which shows you the previous details of robots.txt file.

The Module allows you to view/restore the previous versions of robots.txt.

INSTALLATION
------------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Goto /admin/modules and ENABLE "robotslogs".

To check if it was succesful, goto admin/config > Search and Metadata > RobotsTxt
